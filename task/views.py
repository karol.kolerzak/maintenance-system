from rest_framework import viewsets, generics
from .models import TaskCategory, Task, UsedSpareParts, UserHistory
from .serializers import TaskCategorySerializer, TaskSerializer, UsedSparePartsSerializer, UserHistorySerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from django.utils import timezone
from rest_framework.response import Response
from machines.models import SpareParts
from rest_framework.permissions import IsAuthenticated

class TaskCategoryListCreateView(generics.ListCreateAPIView):
    queryset = TaskCategory.objects.all()
    serializer_class = TaskCategorySerializer


class TaskCategoryDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = TaskCategory.objects.all()
    serializer_class = TaskCategorySerializer


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.filter(active=True)
    serializer_class = TaskSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @action(detail=True, methods=['GET'])
    def login(self, request, **kwargs):
        user = request.user
        task = self.get_object()
        task.doing_by = user
        task.start_work = timezone.now()
        task.save()

        user_task = UserHistory.objects.create(
            user = user,
            task = self.get_object(),
            start_work = timezone.datetime.now(),
            active = True,
            machines = task.machines
        )
        user_task.save()

        serializer_data = TaskSerializer(task).data
        return Response(serializer_data)

    @action(detail=True, methods=['POST'])
    def logout(self, request, **kwargs):
        data = request.data
        user  = request.user
        user_task = user.user_history.filter(active=True).first()
        user_task.closed_at = timezone.now()
        user_task.active = False
        user_task.save()

        task = self.get_object()
        if task.doing_by == user:
            task.doing_by = None

            info = data.get('info', '')
            task.info = f'\n{timezone.now().replace(microsecond=0, tzinfo=None)}: {user.username}: {info}'

            finish = data.get('finish', False)
            if finish == True:
                task.closed_at = timezone.now()
                task.active = False

            new_spare_parts = data.get('spare_parts', [])
            for item in new_spare_parts:
                for k, v in item.items():
                    spare_part = SpareParts.objects.get(pk=k)
                    if spare_part.amount >= v:
                        UsedSpareParts.objects.create(user=user, spare_part=spare_part, task=task, amount=v)
                        spare_part.amount -= v
                    else:
                        spare_part.amount = 0
                    spare_part.save()

            task.save()
            serializer_data = TaskSerializer(task).data


            return Response(serializer_data)
        return Response({'info': "Nie jesteś zalogowany do zadania"})


class UsedSpartePartsView(generics.ListAPIView):
    queryset = UsedSpareParts.objects.all()
    serializer_class = UsedSparePartsSerializer

class UsedSpartePartsSingleView(generics.RetrieveAPIView):
    queryset = UsedSpareParts.objects.all()
    serializer_class = UsedSparePartsSerializer


class UserHistoryView(generics.ListAPIView):
    queryset = UserHistory.objects.all()
    serializer_class = UserHistorySerializer

class UserHistorySingleView(generics.RetrieveAPIView):
    queryset = UserHistory.objects.all()
    serializer_class = UserHistorySerializer