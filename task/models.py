from django.db import models
from machines.models import Machine, SpareParts
from django.conf import settings

User = settings.AUTH_USER_MODEL
class TaskCategory(models.Model):
    code = models.CharField(max_length=5, unique=True, primary_key=True, blank=False, null=False)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.pk


class Task(models.Model):
    category = models.ForeignKey(TaskCategory, on_delete=models.CASCADE)
    machines = models.ForeignKey(Machine, on_delete=models.CASCADE, blank=False, null=False)
    doing_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, default=None, related_name="user_task")
    description = models.TextField(blank=False)
    info = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    start_work = models.DateTimeField(blank=True, null=True, default=None)
    closed_at = models.DateTimeField(blank=True, null=True, default=None)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.pk}"

class UsedSpareParts(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, default=None)
    spare_part = models.ForeignKey(SpareParts, on_delete=models.SET_NULL, null=True, blank=False)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, blank=False, null=False)
    amount = models.PositiveIntegerField(blank=False)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.spare_part} | {self.task} | {self.amount}"

class UserHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, default=None,
                                 related_name="user_history")
    task = models.ForeignKey(Task, on_delete=models.CASCADE, blank=False, null=False)
    start_work = models.DateTimeField(blank=True, null=True, default=None)
    closed_at = models.DateTimeField(blank=True, null=True, default=None)
    active = models.BooleanField(default=True)
    machines = models.ForeignKey(Machine, on_delete=models.CASCADE, blank=False, null=False)
