from rest_framework import serializers
from .models import TaskCategory, Task, UsedSpareParts, UserHistory


class TaskCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskCategory
        fields = '__all__'


class UsedSparePartsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsedSpareParts
        fields = '__all__'


class UserHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserHistory
        fields = '__all__'


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        fields = '__all__'

