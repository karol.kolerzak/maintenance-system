from django.contrib import admin
from .models import TaskCategory, Task, UsedSpareParts, UserHistory

admin.site.register(TaskCategory)
admin.site.register(Task)
admin.site.register(UsedSpareParts)
admin.site.register(UserHistory)