from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'', views.TaskViewSet, basename='task', )


urlpatterns = [
    path('category/<str:pk>', views.TaskCategoryDetailView.as_view()),
    path('category/', views.TaskCategoryListCreateView.as_view()),
    path('used_spare_parts/', views.UsedSpartePartsView.as_view()),
    path('used_spare_parts/<int:pk>', views.UsedSpartePartsSingleView.as_view()),
    path('user_history/', views.UserHistoryView.as_view()),
    path('user_history/<int:pk>', views.UserHistorySingleView.as_view()),
    path('', include(router.urls)),

]