from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token
from . import views


urlpatterns = [
    path('auth/', obtain_auth_token),
    path('register/', views.CreateUserView.as_view())
]