from django.db import models
from django.contrib.auth.models import AbstractUser


class Position(models.Model):
    name = models.CharField(max_length=100, blank=False)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

class User(AbstractUser):
    position = models.ForeignKey(Position, on_delete=models.CASCADE, blank=True, null=True)
    profile_pic = models.ImageField(default='avatar.png')
    extra_info = models.TextField(blank=True)

    def __str__(self):
        return f'{self.username} {self.last_name} {self.first_name}'


