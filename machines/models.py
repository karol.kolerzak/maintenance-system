from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Machine(models.Model):
    name = models.CharField(max_length=100, blank=False, unique=True)
    description = models.TextField(blank=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, to_field='name', default='Others')
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class SpareParts(models.Model):
    name = models.CharField(max_length=100, blank=False)
    description = models.TextField(blank=True)
    machines = models.ManyToManyField(Machine, blank=True, related_name="spare_parts")
    machine_category = models.ManyToManyField(Category, blank=True, null=True, default=None, related_name='spare_parts')
    amount = models.PositiveIntegerField(default=0, blank=True)
    active = models.BooleanField(default=True)
    position = models.CharField(max_length=10, blank=False, default=None)


    def __str__(self):
        return self.name
