from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import Category, Machine, SpareParts

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class MachineSerializer(serializers.ModelSerializer):
    spare_parts = serializers.SerializerMethodField(read_only=True)
    url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Machine
        fields = '__all__'

    def get_spare_parts(self, obj):
        machine_spare_parts = obj.spare_parts.all()

        try:
            machine_spare_parts |= obj.category.spare_parts.all()
        except AttributeError:
            pass

        spare_parts_data = SparePartsSerializer(machine_spare_parts, many=True).data
        request = self.context.get('request')
        spare_parts = []

        for item in spare_parts_data:
            spare_part_dict = {
                'id': item['id'],
                'name': item['name'],
                'url': reverse('spare_parts-detail', kwargs={"pk": item['id']}, request=request)
            }
            spare_parts.append(spare_part_dict)

        return spare_parts

    def get_url(self, obj):
        request = self.context.get('request')
        if request is None:
            return None
        return reverse('machine_detail', kwargs={"pk": obj.pk}, request=request)


class SparePartsSerializer(serializers.ModelSerializer):

    class Meta:
        model = SpareParts
        fields = '__all__'