from django.contrib import admin
from .models import Category, Machine, SpareParts

admin.site.register(Category)
admin.site.register(Machine)
admin.site.register(SpareParts)