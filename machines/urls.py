from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'', views.SparePartsViewSet, basename='spare_parts', )


urlpatterns = [
    path('category/', views.CategoryListCreateView.as_view(), name="category"),
    path('category/<int:pk>', views.CategoryRetrieveUpdateDestroyView.as_view(), name="category_detail"),
    path('', views.MachineListCreateView.as_view(), name="machine"),
    path('<int:pk>', views.MachineRetrieveUpdateDestroyView.as_view(), name="machine_detail"),
    path('spareparts/', include(router.urls)),
]