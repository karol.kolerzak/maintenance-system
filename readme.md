# Maintenance System - not finished yet.
This app allows for the management of a production factory, especially the maintenance department where I work as a Maintenance Engineer. We use similar platform in our work. In this app We can create accounts for employees and assign them to positions within the company. For example: Operator, Mechanic, Engineer, Menager. Depending on the position, employees have different actions they can do. For example, an Engineer can manage a machine, so they can add a machine to the system and add informations and spare parts to it.  Each employee can add work orders, for example, "Engine on Machine 101040 is not working." After that, a mechanic can log in to this order and make the necessary repairs. Once the repairs are completed, the mechanic logs out of the work order, fills out a description of the work done, and assigns any spare parts that were used.

## Table of contents
* [Technologies](#technologies)
* [Install](#install)
* [Documentation](#documentation)

## Technologies
* Python
* DjangoREST
* SQLite


## Install 
* Create virtual environment and activate it
```
$: virtualenv venv
$: source venv/bin/activate
```

* Clone git repository
```
$: git clone https://gitlab.com/karol.kolerzak/maintenance-system
```

* Install requirements
```
$: pip install -r requirements.txt
```

* Make migrations
```
$: python manage.py makemigrations
$: python manage.py migrate
```
